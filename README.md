# Rahlfs LXX USFM

1935 Rahlfs LXX as USFM

## Name
Rahlfs LXX USFM

## Description
This project is an attempt to publish the [1935 Rahlfs LXX](https://el.wikisource.org/wiki/%CE%97_%CE%A0%CE%B1%CE%BB%CE%B1%CE%B9%CE%AC_%CE%94%CE%B9%CE%B1%CE%B8%CE%AE%CE%BA%CE%B7_(Rahlfs))as a set of .usfm files for Bible translation. This Septuagint edition is not to be confused with the [2006 Rahlfs-Hanhart LXX](https://www.academic-bible.com/en/online-bibles/septuagint-lxx/read-the-bible-text/), which is a later revision based on the 1935 Rahlfs LXX.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
This set of .usfm files can be used in software such as [Bibledit](bibledit.org) or [Paratext](https://paratext.org/). Users might need to remove alternative duplicate books that Rahlfs included, namely
* Joshua (Codex Alexandrinus)
* Judges (Codex Alexandrinus)
* Tobit (Codex Sinaiticus)
* Susanna (Theodotion)
* Bel and the Dragon (Theodotion)
* Daniel (Theodotion)

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
The project is now finished.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Thank you to all of the contributors to this project! May the Lord God remember you in his kingdom always, now and ever, and unto ages of ages, amen.

## License
The original text "[Η Παλαιά Διαθήκη (Rahlfs)](https://el.wikisource.org/wiki/%CE%97_%CE%A0%CE%B1%CE%BB%CE%B1%CE%B9%CE%AC_%CE%94%CE%B9%CE%B1%CE%B8%CE%AE%CE%BA%CE%B7_(Rahlfs))" is licensed under a [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/) license. It is attributed to [Alfred Rahlfs](https://en.wikipedia.org/wiki/Alfred_Rahlfs), with publishing and editing by [Wikisource](wikisource.org) and Wikisource contributors. / Converted to .usfm format from original plaintext.
Modified content licensed under [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/).

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
